import React, { Component } from 'react';
import { Grid } from 'react-bootstrap';

//import router from React
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

//import Sass CSS from Assets folder 
import './assets/css/default.min.css';

//Component import can be done here
import Header from './components/headerComponent/header';
import Footer from './components/footerComponent/footer';
import Home from './components/pages/home/home';
import Services from './components/pages/services/services';
import ContactUs from './components/pages/contactUs/contactUs';
import Tumblr from './components/pages/blog/tumblr';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Grid>
        	 <Header/>
              <Route exact path="/" component={Home} />
              <Route exact path="/services" component={Services} />
              <Route exact path="/blog" component={Tumblr} />
              <Route exact path="/contactUs" component={ContactUs} />
            <Footer/>
          </Grid>
        </Router>
      </div>
    );
  }
}

export default App;
