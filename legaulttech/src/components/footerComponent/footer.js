import React, { Component } from 'react';
import { Row, Col, Popover, OverlayTrigger } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';


//Components must start with an uppercase letter
class Footer extends Component {
	render() {
		const popoverLeft = (
		  <Popover id="popover-positioned-left">
		    <strong>Phone Number to reach us.</strong>
		  </Popover>
		);


		return (
			<footer>
				<Row>
					<hr/>
					<Col xs={6} sm={6} md={6}>
						<p>Contact: <a href="mailto:patrique.legault@gmail.com">Patrique Legault</a>.</p>
						<div className="footerFonts">
							<Col xs={2} sm={2} md={2}>
								<a href="https://www.linkedin.com/in/patrique-legault" target="_blank" rel="noopener noreferrer"><FontAwesome
									name=''
							        className='fa-linkedin-square'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', fontSize: '4vw' }}
							    ></FontAwesome></a>
							</Col>
							<Col xs={2} sm={2} md={2}>
								<a href="https://www.facebook.com/Legault.Tech" target="_blank" rel="noopener noreferrer"><FontAwesome
									name=''
							        className='fa-facebook-square'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', fontSize: '4vw' }}
							    ></FontAwesome></a>
							</Col>
							<Col className="hidden-xs" xs={2} sm={2} md={2}>
								<a href="/blog"><FontAwesome
									name=''
							        className='fa-tumblr'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', fontSize: '4vw' }}
							    ></FontAwesome></a>
							</Col>
							<Col className="hidden-sm hidden-md hidden-lg" xs={2} sm={2} md={2}>
								<a href="https://legaulttech.tumblr.com" target="_blank" rel="noopener noreferrer"><FontAwesome
									name=''
							        className='fa-tumblr'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', fontSize: '4vw' }}
							    ></FontAwesome></a>
							</Col>
						</div>
					</Col>
					<Col xs={6} md={6}>
						<address>
						  <strong>Legault.Tech</strong><br/>
						  221 Lyon Street N.,<br/>
						  Ottawa ON,<br/>
						  K1R 7X5<br/>
						  <OverlayTrigger trigger="hover" placement="left" overlay={popoverLeft}>
						  	<div><abbr title="Phone">P:</abbr> (613) 914-2477</div>
						   </OverlayTrigger>
						</address>
					</Col>
				</Row>
			</footer>
		);
	}
}

export default Footer;