import React, { Component } from 'react';
import { Row, Col, Nav, NavItem, Navbar} from 'react-bootstrap';
import Img from '../imgComponent/img';

//Components must start with an uppercase letter
//Very good link https://stackoverflow.com/questions/43010814/how-can-i-make-react-bootstraps-dropdown-open-on-mouse-hover
class Header extends Component {

	render() {

		return (
			<header>
				<Row className="show-grid">
					<Col xs={12} md={12}>
						
						{/* Home */}
					{/* My Tumblr App Link https://www.tumblr.com/open/app?referrer=tumblr_new_iframe&app_args=blog%3FblogName%3Dlegaulttech%26page%3Dblog */}
						 <Navbar>
						    <Navbar.Header>
						      <Navbar.Brand>
						      	<div className="top">
						       		<a href="/">
						       			<Img className="img-responsive center-block"/>
						       		</a>
						        </div>
						      </Navbar.Brand>
						      <Navbar.Toggle />
						    </Navbar.Header>
						    <Navbar.Collapse>
						      <Nav pullRight>
						        <NavItem eventKey="1" href="/">Home</NavItem>
						        <NavItem eventKey="2" href="/services">Services</NavItem>
						        <NavItem className="hidden-xs" eventKey="3" href="/blog">Blog</NavItem>
						        <NavItem className="hidden-sm hidden-md hidden-lg" eventKey="4" href="https://legaulttech.tumblr.com" target="_blank">Blog</NavItem>
						        <NavItem eventKey="5" href="/contactUs">Contact Us</NavItem>
						      </Nav>
						    </Navbar.Collapse>
						  </Navbar>
		      		</Col>
				</Row>
			</header>
		);
	}
}

export default Header;
