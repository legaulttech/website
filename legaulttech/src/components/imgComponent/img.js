import React, { Component } from 'react';
import img from '../../assets/img/logo.png';

//Components must start with an uppercase letter
class Img extends Component {
  render() {
    return (
    	<img src={img} alt="Legault.Tech" height="120px" width="125px"/>
    );
  }
}

export default Img;
