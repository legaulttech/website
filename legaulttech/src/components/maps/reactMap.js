import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';

class ReactMap extends Component {


	constructor(props) 
	{
	    super(props);
	    this.state = {
	      showingInfoWindow: false,
	      activeMarker: {},
	      selectedPlace: {},
	    }
	    
	    // binding this to event-handler functions
	    this.onMarkerClick = this.onMarkerClick.bind(this);
	    this.onMapClicked = this.onMapClicked.bind(this);
    }
  
	onMarkerClick(props, marker, e) {
		this.setState({
		  selectedPlace: props,
		  activeMarker: marker,
		  showingInfoWindow: true
		});
	}

	onMapClicked(props) {
		if (this.state.showingInfoWindow) {
			this.setState({
			showingInfoWindow: false,
			activeMarker: null
			})
		}
	}

	stylizeInfoWindw() {
		return <h4><i>Legault.Tech</i></h4>; 
	}

	render() {

		const style = {
			width: 'auto',
			height: 'auto',
			overflow: 'hidden',
			margin: 0,
			padding: 0
			
		}

		return (
			  <Map google={this.props.google}
			  	style={style}
			  	zoom={15}
			  	options={this.createMapOptions}
			  	scrollwheel={false}
			  	initialCenter={{
		            lat: 45.41664350000001,
		            lng: -75.70281549999999
        		}}
        		styles={[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#525e63"},{"visibility":"on"}]}]}
			 	>
			 		<Marker onClick={this.onMarkerClick}
					    title={'Legault.Tech Office'}
					    name={this.stylizeInfoWindw()}
					    position={{lat: 45.41664350000001, lng: -75.70281549999999}} />

					<InfoWindow
				      marker={this.state.activeMarker}
				      visible={this.state.showingInfoWindow}>
				        <div>
				          <h1>{this.state.selectedPlace.name}</h1>
				        </div>
				    </InfoWindow>
			  </Map>
		);
	}
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDdXOsoUpttAoUW-CSxnqVwotg_n2gHIB8"
})(ReactMap)
