import React, { Component } from 'react';

//Components must start with an uppercase letter
class Tumblr extends Component {

	render() {

		return (
			<div className="blog">
			    <embed type="text/html" src="https://legaulttech.tumblr.com/" style={{width: '100%', height: '1000px'}}/>
			</div>
		);
	}
}

export default Tumblr;