import React, { Component } from 'react';
import { Row, form, FormGroup, Col, ControlLabel, FormControl, Button, Alert } from 'react-bootstrap';
import axios from 'axios';
import ReactMap from '../../maps/reactMap';
import ScrollableAnchor from 'react-scrollable-anchor';

{/* https://github.com/mzabriskie/axios */}
class ContactUs extends Component {

	//Initialize the state of the object
	constructor(props) {
		super(props);

		this.state = {
			name: '', 
			email : '',
			number: '',
			profession: '',
			other: '',
			comments: '',
			professionSelection : '',
			clicked: false,
			nameValidation : 'error',
			emailValidation: 'error',
			numberValidation: 'error',
			professionValidation: 'error',
			commentValidation: 'error',
			remainingWords: '15 words remaining'
		};

		this.onChange = this.onChange.bind(this);
		this.sendFormData = this.sendFormData.bind(this);
	}

	//Bind to the change event of the form fields
	onChange(e) {

  		this.setState({[e.target.name]: e.target.value});

  		if(e.target.name === "email") {
  			var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  			if(re.test(e.target.value))
  			{
  				this.setState({emailValidation: 'success'});
  			}
  			else {
  				this.setState({emailValidation: 'error'});	
  			}
  		}

  		if(e.target.name === "name") {
  			if(e.target.value.length > 2)
  			{
  				this.setState({nameValidation: 'success'});	
  			}
  			else
  			{
  				this.setState({nameValidation: 'error'});	
  			}
  		}

  		if(e.target.name === "number") {

  			re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;

  			if(re.test(e.target.value))
  			{
  				this.setState({numberValidation: 'success'});	
  			}
  			else
  			{
  				this.setState({numberValidation: 'error'});	
  			}
  		}

  		if(e.target.name === "profession") {

  			this.setState({professionSelection: e.target.value});
  			
  			this.setState({professionValidation: 'success'});	

  		}

  		if(e.target.name === "comments") {

  			if(e.target.value.trim().split(/\s+/).length >= 15)
  			{
  				this.setState({commentValidation: 'success', remainingWords: "Alright that is good enough"});
  			}
  			else
  			{
  				var result = 15 - e.target.value.split(" ").length + 1;
  				this.setState({commentValidation: 'error', remainingWords: result + " words remaining"});	
  			}

  		}
	}

	sendFormData(props) {
		//Make http POST request to formspree in order to send the data over

		if(this.state.nameValidation === "success" && this.state.emailValidation === "success" && this.state.numberValidation === "success" && this.state.professionValidation === "success" && this.state.commentValidation === "success")
		{

			if(this.state.profession !== "Other")
			{
				axios.post('//formspree.io/patrique.legault@gmail.com', {
				    name: this.state.name,
				    email: this.state.email,
				    number: this.state.number,
				    profession: this.state.profession,
				    comments : this.state.comments
			  	})
			  	.then(function (response) {
			    	console.log(response);
			  	})
			  	.catch(function (error) {
			    	console.log(error);
			  	});
			}
			else
			{
				axios.post('//formspree.io/patrique.legault@gmail.com', {
				    name: this.state.name,
				    email: this.state.email,
				    number: this.state.number,
				    profession: this.state.profession,
				    other: this.state.other,
				    comments : this.state.comments
			  	})
			  	.then(function (response) {
			    	console.log(response);
			  	})
			  	.catch(function (error) {
			    	console.log(error);
			  	});
			}

		  	//Clear the form field values
			this.setState({
				name: '',
				number: '',
		    	email: '',
		    	profession: '',
		    	comments: '',
		    	clicked: true,
		    	nameValidation : 'error',
				emailValidation: 'error',
				numberValidation: 'error',
				professionValidation: 'error',
				commentValidation: 'error',
				remainingWords: '20 words remaining'
			});


			setTimeout(function() {
				this.setState({clicked: false}); }.bind(this), 
			9000);

		}
		
	}

	render () {

		return (
			<div className="contactUs">
				<Row>
					<span><i><b>Have an inquiry, leave us a message and someone will get back to you.</b></i></span>
					<ScrollableAnchor id={'form'}>
						<div className="form">
							<form>
								
								{/* Name field */}
								<FormGroup controlId="formHorizontalEmail" validationState={this.state.nameValidation}>
								    <Col componentClass={ControlLabel} xs={9} md={2} sm={2}>
								        Name
								    </Col>
									<Col xs={11} sm={9} md={9}>
								    	<FormControl value={this.state.name} onChange={this.onChange} name="name" type="text" placeholder="Name"/>
								    	<FormControl.Feedback />
								  	</Col>
								</FormGroup>


								{/* Email field */}
								<FormGroup controlId="formHorizontalEmail" validationState={this.state.emailValidation}>
								    <Col componentClass={ControlLabel} xs={9} md={2} sm={2}>
								        Email
								    </Col>
									<Col xs={11}sm={9} md={9}>
								    	<FormControl value={this.state.email} onChange={this.onChange} name="email" type="email" placeholder="Email (i.e) patrique.legault@gmail.com" />
								    	<FormControl.Feedback />
								  	</Col>
								</FormGroup>

								{/* Phone Number field */}
								<FormGroup controlId="formHorizontalEmail" validationState={this.state.numberValidation}>
								    <Col componentClass={ControlLabel} xs={9} sm={2} md={2}>
								        Number
								    </Col>
									<Col xs={11} sm={9} md={9}>
								    	<FormControl value={this.state.number} onChange={this.onChange} name="number" type="text" placeholder="Phone Number (i.e) 613-914-2477" />
								    	<FormControl.Feedback />
								  	</Col>
								</FormGroup>

								{/* Career title */}
								<FormGroup controlId="formControlsSelect" validationState={this.state.professionValidation}>
									<Col componentClass={ControlLabel} xs={9} md={2} sm={2}>
							    		<ControlLabel>Profession</ControlLabel>
							    	</Col>
							    	<Col xs={11} sm={9} md={9}>
							      		<FormControl value={this.state.profession} onChange={this.onChange} name="profession" componentClass="select" placeholder="select">
							        		<option value="Marketer">Marketer</option>
							        		<option value="Developer">Developer</option>
							        		<option value="Architect">Architect</option>
							        		<option value="Engineer">Engineer</option>
							        		<option value="Systems Analyst">Systems Analyst</option>
							        		<option value="Student">Student</option>
							        		<option value="Other">Other</option>
							      		</FormControl>
							      		<FormControl.Feedback />
							      	</Col>
							    </FormGroup>

							    {/* Other field */}
								<FormGroup className={this.state.professionSelection === "Other" ? '' : 'specify'}  controlId="formHorizontalOther">
								    <Col componentClass={ControlLabel} xs={9} md={2} sm={2}>
								        Specify
								    </Col>
									<Col xs={11} sm={9} md={9}>
								    	<FormControl value={this.state.other} onChange={this.onChange} name="other" type="text" placeholder="Current Career" />
								  	</Col>
								</FormGroup>

								{/* Comments field */}
								<FormGroup controlId="formControlsTextarea" validationState={this.state.commentValidation}>
									<Col xs={9} sm={2} md={2}>
							      		<ControlLabel>Comments</ControlLabel>
							      	</Col>
							      	<Col xs={11} sm={9} md={9}>
							      		<FormControl value={this.state.comments} onChange={this.onChange} name="comments" componentClass="textarea" placeholder="Leave us a comment" />
							      		<FormControl.Feedback />
							      		<Col xs={12}  sm={10} md={11}>
								      		<div className="wordsRemaining">
								      			<span><center>{this.state.remainingWords}</center></span>
								      		</div>
							      		</Col>
							      	</Col>
							    </FormGroup>

								{/* Submit button */}
								<FormGroup>
							      <Col xs={11} sm={11} md={12}>
							        <Button className="centerButton" onClick={this.sendFormData} bsSize="large">
							          Send
							        </Button>
							      </Col>
							    </FormGroup>
							</form>
						</div>
					</ScrollableAnchor>
					<div>
						<Col xs={12} sm={12} md={12}>
							<div className={ this.state.clicked === true ? 'thankYou2' : 'thankYou1' }>
								<Alert bsStyle="warning">
									<strong>Thank you for submitting your request. Someone will get to you shortly.</strong>
								</Alert>
							</div>
						</Col>
					</div>
				</Row>
				<Row>
					<ScrollableAnchor id={'map'}>
						<div className="map">
							<span><i><b>Visit our location</b></i></span>
							<ReactMap/>
						</div>
					</ScrollableAnchor>
				</Row>
			</div>
		);
	}

}

export default ContactUs;