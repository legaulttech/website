import React, { Component } from 'react';
import { Row, Col, ListGroup, ListGroupItem, Button, Modal, Popover, OverlayTrigger, Image } from 'react-bootstrap';
import ScrollableAnchor from 'react-scrollable-anchor';
import FontAwesome from 'react-fontawesome';
import ModalText from './modalText'


//Images to import
import TutorDoctor from '../../../assets/img/tutorDoctor.png';
import Laurentian from '../../../assets/img/laurentian.jpg';
import OttawaU from '../../../assets/img/OttawaU.jpeg';
import FourPoint from '../../../assets/img/4Point.png';
import AFTIA from '../../../assets/img/aftia.jpg';

//Components must start with an uppercase letter
{/* https://www.npmjs.com/package/react-scrollable-anchor */}
class Home extends Component {

  	constructor(props) {
  		super(props);
  		this.state = {show: false, modalContent : "tutorDoctor"};
  	}

  	renderText(text) {

  		//Get the text from the modal text component
  		var result = <ModalText type={text}/>;
  		
  		return result;
  	}

	render () {

		let close = () => this.setState({ show: false});

		const popoverRight = (
		  <Popover id="popover-positioned-bottom">
		    <strong>Click Me!</strong>
		  </Popover>
		);

		const WWAText = 
			(<span><center>Specializing in the hightech market and providing clients with the best knowledge in regards to current market trends and standards.
				I can guarantee our clients the following three aspects <strong>Mobility</strong>, <strong>Hard Work</strong> & <strong>Promising Results</strong>
				
				<br/>
				<br/>
				
				<ListGroup>
				    <ListGroupItem href="#"><strong><u>Mobility</u></strong> I will provide our clients with in house visits, making it easier to provide clients with my services.</ListGroupItem>
				    <ListGroupItem href="#"><strong><u>Hard Work</u></strong> is something that <i>Legault.Tech</i> promises to all clients no matter what the problem.</ListGroupItem>
					<ListGroupItem href="#"><strong><u>Promising Results</u></strong> with a combination of the two prior items the only thing that clients will see is promising results</ListGroupItem>
				</ListGroup>
				</center></span>);


			const EducationText = 
				(
					<span><center>Education is one thing that <strong>Legault.Tech</strong> offers</center></span>);


		return (
			<div>
				<ScrollableAnchor id={'wwa'}>
					<div className="wwa">
						<center><h2>Who We Are</h2></center>
						<Row>
							<pre>{WWAText}</pre>
							<Col xs={4} md={4} className="center-text">
								<div className="icons center-block">
									<i className="material-icons moveLeft">directions_walk</i>
									<span>Mobile</span>
								</div>
							</Col>
							<Col xs={4} md={4} className="center-text">
								<div className="icons center-block">
									<i className="material-icons moveLeft">work</i>
									<span>Work</span>
								</div>
							</Col>
							<Col xs={4} md={4} className="center-text">
								<div className="icons center-block">
									<i className="material-icons moveLeft">insert_emoticon</i>
									<span>Results</span>
								</div>
							</Col>
						</Row>
					</div>
				</ScrollableAnchor>

				<ScrollableAnchor id={'education'}>
					<div className="education">
						<center><h2>Education</h2></center>
						<Row>
							<pre>{EducationText}</pre>
							<Col xs={6} md={6}>
								<FontAwesome
									name=''
							        className='fa-graduation-cap'
							        size='5x'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
							    >
							    </FontAwesome>
							    <p><center><em><strong>Double Major in Mathematics and Computer Science</strong></em><br/>at<br/><a href="https://laurentian.ca/program/computer-science" target="_blank">Laurentian University</a></center></p>
							</Col>
							<Col xs={6} md={6}>
								<FontAwesome
									name=''
							        className='fa-graduation-cap'
							        size='5x'
							        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
							    >
							    </FontAwesome>
							    <p><center><em><strong>Masters in Electronic Business Technology Specializing in Enterprise Technology</strong></em><br/>at<br/><a href="http://catalogue.uottawa.ca/en/graduate/master-electronic-business-technologies/" target="_blank">The University of Ottawa</a></center></p>
							</Col>
						</Row>
						<Row>
							<Col xs={4} md={4}>
								<FontAwesome
									name=''
									className="fa-certificate"
									size='5x'
									style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
								/>
								<p><center><em><strong>Adobe Certified Business Practitioner</strong></em></center></p>
							</Col>
							<Col xs={4} md={4}>
								<FontAwesome
									name=''
									className="fa-certificate"
									size='5x'
									style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
								/>
								<p><center><em><strong>Adobe Certified AEM Developer</strong></em></center></p>
							</Col>
							<Col xs={4} md={4}>
								<FontAwesome
									name=''
									className="fa-certificate"
									size='5x'
									style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)' }}
								/>
								<p><center><em><strong>Adobe AEM Forms Developer</strong></em></center></p>
							</Col>
						</Row>
						<Row>
							<Col xs={12} sm={12} md={12}>
								<div>
									<pre>View my <a href="https://drive.google.com/open?id=0BwTIsOfTkQEZYkpxWnRLYTBUMTA" target="_blank">resume</a> for more information on what I have accomplished</pre>
								</div>
							</Col>
						</Row>
					</div>
				</ScrollableAnchor>

				<ScrollableAnchor id={'experience'}>
					<div className="experience">
						<center className="spacer"><h2>Experience</h2></center>
						<Row>
							<Col xs={12} sm={12} md={12}>
								<div className="hoverExperience" onClick={() => this.setState({ show: true, modalContent: "Laurentian" })}>
									<OverlayTrigger trigger="hover" placement="bottom" overlay={popoverRight}>
										<Image src={Laurentian} alt="Laurentian University" responsive/>
									</OverlayTrigger>
								</div>
							</Col>
						</Row>

						<hr width="75%"/>

						<Row>
							<Col xs={12} sm={12} md={12}>
								<div className="hoverExperience" onClick={() => this.setState({ show: true, modalContent: "TutorDoctor"})}>
									<Image src={TutorDoctor} alt="Tutor Doctor" responsive/>
								</div>
							</Col>

						</Row>

						<hr width="75%"/>

						<Row>
							<Col xs={12} sm={12} md={12}>
								<div className="hoverExperience" onClick={() => this.setState({ show: true, modalContent: "OttawaU"})}>
									<Image src={OttawaU} alt="The University of Ottawa" responsive/>
								</div>
							</Col>
						</Row>

						<hr width="75%"/>

						<Row>
							<Col xs={12} sm={12} md={12}>
								<div className="hoverExperience" onClick={() => this.setState({ show: true, modalContent: "FourPoint" })}>
									<Image src={FourPoint} alt="Four Point Solutions" responsive/>
								</div>
							</Col>
						</Row>

						<hr width="75%"/>

						<Row>
							<Col xs={12} sm={12} md={12} className="spacer">
								<div className="hoverExperience"  onClick={() => this.setState({ show: true, modalContent: "AFTIA"})}>
									<Image className="aftia" src={AFTIA} alt="AFTIA Solutions" responsive/>
								</div>
							</Col>
						</Row>

						<div className="modal-container" style={{height: 200}}>
					        <Modal
					          show={this.state.show}
					          onHide={close}
					          container={this}
					          aria-labelledby="contained-modal-title"
					        >
					          <Modal.Header closeButton>
					            <Modal.Title id="contained-modal-title">Work Experience</Modal.Title>
					          </Modal.Header>
					          <Modal.Body>
					            {this.renderText(this.state.modalContent)}
					          </Modal.Body>
					          <Modal.Footer>
					            <Button onClick={close}>Close</Button>
					          </Modal.Footer>
					        </Modal>
					    </div>
					</div>
				</ScrollableAnchor>
			</div>
		);
	}
}

export default Home;