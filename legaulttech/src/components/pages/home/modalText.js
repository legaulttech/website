import React, { Component } from 'react';

class Temp extends Component {

	getText(text) {

		var result = "";

		switch(text) {

			case "TutorDoctor":
				result = <strong>Working with <a href="http://www.tutordoctor.com/" target="_blank">Tutor Doctor</a>, I tutored multiple students in Mathematics and Computer Science. I would create mock exams prior to in order to test the minds of the students. The greatest benefit to this is that students saw a gain in confidence. They realize that they can resolve problems of similar strength on their own. Students get to learn in their own home during their own availability, allowing them a sense of comfort and stability.</strong>
				break;

			case "Laurentian":
				result = <strong>Working at <a href="https://laurentian.ca/program/computer-science" target="_blank">Laurentian University</a> I worked in the Mathematics Clinic tutoring students: Calculus, Linear Algebra, Discrete Mathematics and Differential Equations. We had a classroom for students to ask questions and provide them with the help they needed to pass their midterms and finals. We quickly learned that teachng these concepts was easiest with examples providing students with step by step directions on how to derive the correct solution. <br/> <br/> During this time I also worked with professors on some of their projects. The biggest project was using Mathematica to derive a three dimensional simulation of various atoms interacting with each other. We would did this in order to calculate their energies at various vibrations and stresses. Here are some <a href="https://drive.google.com/open?id=0BwTIsOfTkQEZZ3gtRkJuZnRTTmc" target="_target">of their words</a> given to me by <a href="https://drive.google.com/open?id=0BwTIsOfTkQEZZW1jaU5BRzcxREk" target="_target">the professors</a> from the Mathematics and Computer Science faculty at Laurentian University.</strong>
				break;

			case "OttawaU":
				result = <strong>Working with the University of Ottawa I assistend professors with their correction and labs. One of the labs that I assisted was introduction to Java where the University held an interactive Java tutorial every Thursday for students. There I would teach students various concepts such as control structures, static functions, recursion and many more features of the Java language. Another class that I aided in was Discrete Mathematics, there I answered questions, did mathematical proofs with the students. Prior to completing my post graduate degree I completed my report entitled <i>Predicting the success of antidepressant treatment for depressed patients using MADRS and EEG data</i>. This paper took anonymous patient data and looked for any correlations between EEG signals in the brain and patient success with antidepressants. The complete report can be read <a href="https://drive.google.com/open?id=0BwTIsOfTkQEZdmN3X0RtTlRmYUU" target="_blank">here</a>.</strong>
				break;

			case "FourPoint":
				result = <strong>Working at <a href="http://www.4point.com/" target="_target">4Point Solutions</a> I was part of the suport team, where we helped clients resolve technical problems, ranging from implementations of SSL to troubleshooting, <a href="http://www-03.ibm.com/software/products/en/appserv-was" target="_target">WebSphere</a>, <a href="http://www.oracle.com/technetwork/middleware/weblogic/overview/index.html" target="_target">WebLogic</a> or <a href="http://www.theserverside.com/definition/JBoss" target="_target">JBoss</a> application servers running LiveCycle/AEM (Adobe Experience Manager). During this time I learned various concepts such as high availability, web services (SOAP vs. REST), non relational databases to relational databases. Many of times I did not just have to learn the concepts I had to deliver a solution that required me to use those concepts. We also aided with the professional services department, implementing custom solutions with the AEM application.</strong>
				break;

			case "AFTIA":
				result = <strong>Working at <a href="http://www.aftia.com/" target="_blank">AFTIA Solutions</a> I helped with prepering client demonstrations of the Adobe Experience Manager platform. Working with Sales and Services, we gathered client requirements and derived a user story that we would demonstrate to the cient. We also aided in various AEM installations, and implementations. Integrating <a href="https://acrobat.adobe.com/ca/en/sign.html" target="_target">Adobe Sign</a> and AEM Forms in order to automate electronic signatures in the enterprise, reducing costs and lowering document processing time. I have also worked with various other vendors implementing forms that retrieve data from backend systems such as <a href="https://www.pega.com/" target="_target">PEGA</a> and populate the form prior to client viewing it. This concept is known as prepopulation and decreases the amount of time users spend on a form which in return drives the client adoption rates higher.</strong>
				break;

			default : 
				result = <strong>This is nothing</strong>;
		}

		return result;
	}

	render() {

		return (
				<div>
					{this.getText(this.props.type)}
				</div>
			);

	}

}

export default Temp;