import React, { Component } from 'react';
import { Panel, ListGroup, ListGroupItem, Accordion, Table} from 'react-bootstrap';
import ScrollableAnchor from 'react-scrollable-anchor';

class Services extends Component {

	render() {

		const tutoring = (
				<span><center>I offer mobile tutoring services to a wide range of students who need help in Mathematics and Computer Science. Students in primary school, high school and post secondary are all eligable for our services.</center></span>
			);

		const analysis = (
				<span><center>At Legault.Tech analysis is something that I take very seriously, when corporations are looking for change they need expert advice in order to help the organization decide on which path they should take. I guarantee to be there to help them every step along the way. Providing the organization with various skill sets such as, architecting new solutions, to helping the organization deliver them. Using current industry standards and current technologies in order to help stream line any organization from start to finish. Lastly teaching the employees on how to use and extend those technologies in a lean development environment.</center></span>
			);

		const costs = (
				<span><center>I aim to be as affordable as possible, in order to help others with our services. If you have any questions about costs please call our number.</center></span>
			);

		return (
			<div className="services">
				<ScrollableAnchor id={'tutoring'}>
					<div className="tutoring">
						<p><center className="title">Tutoring Services</center></p>
						<pre>{tutoring}</pre>
						<div className="mathCourses">
							<Panel collapsible defaultExpanded header="Mathematics Courses That We Tutor">
							    <ListGroup fill>
							      <ListGroupItem>Calculus</ListGroupItem>
							      <ListGroupItem>Linear Algebra</ListGroupItem>
							      <ListGroupItem>Discrete Mathematics</ListGroupItem>
							      <ListGroupItem>Functions</ListGroupItem>
							      <ListGroupItem>Advanced Functions</ListGroupItem>
							    </ListGroup>
							</Panel>
						</div>

						<div className="mathCourses">
							<Panel collapsible header="Computer Science Topics That We Tutor">
							    <ListGroup fill>
							      <ListGroupItem>Java</ListGroupItem>
							      <ListGroupItem>Databases</ListGroupItem>
							      <ListGroupItem>Internet Technologies</ListGroupItem>
							      <ListGroupItem>Adobe Experience Manager</ListGroupItem>
							    </ListGroup>
							</Panel>
						</div>
					</div>
				</ScrollableAnchor>

				<ScrollableAnchor id={'analyst'}>
					<div className="analyst">
						<p><center className="title">Analyst Services</center></p>
						<pre>{analysis}</pre>

						<div className="analystTopics">
							 <Accordion>
							  	<Panel header="Google Cloud Training" eventKey="1">
							      Many people are not aware of the features their Android phone offers and end up using them in inneficient ways. With our in home services I show you how to use your devices to its full extent, integrating your phone with the following Google cloud services:
								    <div className="googleCloud">
								      <ListGroup>
									    <ListGroupItem><b>Google Photos</b></ListGroupItem>
									    <ListGroupItem><b>Google Assistant</b></ListGroupItem>
									    <ListGroupItem><b>Google Drive</b></ListGroupItem>
		  							  </ListGroup>
		  							</div>
							    </Panel>
							     <pre className="pre-extended">Using the latest technologies such as Git and the Atlassian stack I can provide developers and enterprises with real time code storage and collaboration across the development lifecycle.</pre>
							     <Panel header="Java Bundle Development" eventKey="3">
							      Many Java applications use an OSGi container to keep Java applications independent from each other <br/> (examples below)
							      <div className="googleCloud">
								      <ListGroup>
									    <ListGroupItem><b>Adobe AEM</b></ListGroupItem>
									    <ListGroupItem><b>JBoss</b></ListGroupItem>
									    <ListGroupItem><b>Apache Karaf</b></ListGroupItem>
		  							  </ListGroup>
		  							</div>
							    </Panel>
							    <pre className="pre-extended">
							      With over 5+ years of troubleshooting and installations, I offer a class on how to propperly manage this system and prevent users from running into errors. Learning some of the basic concepts such as log files and monitoring. To learning how to automate backups and use third party data storage containers to host your content repository.<br/><br/>
							      With many years of Business Process Redesign, I can offer professional services in analyzing your current business processes and seeing how they can be streamlined with modern electronic processes.
							    </pre>
							 </Accordion>
						 </div>
					</div>
				</ScrollableAnchor>

				<ScrollableAnchor id={'costs'}>
					<div className="costs">
						<p><center className="title">Costs</center></p>
						<pre>{costs}</pre>

						<div className="tableOverride">
							<Table bordered hover responsive>
								<thead>
								  <tr>
								    <th>Classes</th>
								    <th>Cost</th>
								    <th>Time (minutes)</th>
								    <th>Sessions</th>
								  </tr>
								</thead>
								<tbody>
								  <tr>
								    <td>Mathematics Classes</td>
								    <td>45$</td>
								    <td>60</td>
								    <td>Minimum of 3</td>
								  </tr>
								  <tr>
								    <td>Computer Science Classes</td>
								    <td>45$</td>
								    <td>60</td>
								    <td>Minimum of 3</td>
								  </tr>
								  <tr>
								    <td>Google Cloud Training</td>
								    <td>60$</td>
								    <td>90</td>
								    <td>Minimum of 1</td>
								  </tr>
								  <tr>
								    <td>Source Control & Code Management</td>
								    <td>120$</td>
								    <td>60</td>
								    <td>Minimum of 3</td>
								  </tr>
								  <tr>
								    <td>Java Bundle Development</td>
								    <td>120$</td>
								    <td>60</td>
								    <td>Minimum of 3</td>
								  </tr>
								  <tr>
								    <td>Adobe Experience Manager/LiveCycle System Administration</td>
								    <td colSpan="3">Please inquire for more details</td>
								  </tr>
								  <tr>
								    <td>Enterprise Process Optimization</td>
								    <td colSpan="3">Please inquire for more details</td>
								  </tr>
								</tbody>
							</Table>
						</div>
					</div>
				</ScrollableAnchor>
			</div>
		);
	}

}

export default Services;