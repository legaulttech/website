# Step 1: Add Nodejs PPA
sudo apt-get install python-software-properties
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

# Step 2: Install Nodejs and NPM
sudo apt-get update
sudo apt-get install nodejs

#Step 3: Check Node.js and NPM Version
echo `node -v`
echo `npm -v`

# Step 4: Install nginx https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04
sudo apt-get update
sudo apt-get install -y nginx

# Step 5: Adjust the firewall
sudo ufw allow 'Nginx HTTP'
sudo ufw status

# Step 6: Install node_modules for website
cd /home/vagrant/website
npm install

# Step 7: Make a production ready build of the website
npm run build

# Step 8: Install the server
sudo npm install -g serve

# Step 9: Run the server with caching enabled and silent mode to prevent error messages being written to the browser console
serve -s -S -p 8000 -c 120 build/ &

#Step 10: Setup proxy cache with Nginx
sudo mkdir -p /var/nginx/cache

# Step 11: Setup nginx for reverse proxy
sudo systemctl stop nginx
sudo cp /vagrant_data/localhost /etc/nginx/sites-available/
sudo cp /vagrant_data/default /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/localhost /etc/nginx/sites-enabled/localhost
sudo systemctl start nginx